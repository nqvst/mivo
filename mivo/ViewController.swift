//
//  ViewController.swift
//  mivo
//
//  Created by Jack Enqvist on 2019-06-27.
//  Copyright © 2019 Jack Enqvist. All rights reserved.
//

import UIKit
import Foundation
import AVKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        let myURL = URL(string: "https://absvpvodps-vh.akamaihd.net/2019/03/5c9883d6de54fc1d10a6d714/1280_720_2706.mp4")
        
        let player = AVPlayer(url: myURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
        player.play()

      
    }

}

